################################
# Build and create Artifacts
################################
1. Go to directory "cg_checkout_system" where pom.xml is located and execute maven goal "mvn clean install"
2. This goal creates "cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar" in the target directory.

###################################
To Run Step-1 [With No Offers] 
###################################
1.open command prompt at location where "cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar" was create/placed
2 run command::
java -jar cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar <item1,item2,item3>

Ex.-> D:\workspaces\exercise\cg_checkout_system\target>java -jar cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar apple,orange,apple

3. Logs can be found at:: ${working_directory}/logs/cg_checkout_system.log

###################################
To Run Step-2 [With Offers] 
###################################
1. Sample "offers.conf" is provided with the project in resources directory.
2. please copy it preferably in the same directory as "cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar"
3. Run command:
java -jar cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar <item1,item2,item3> <FilePath or just File Name if in the same Directory>
Ex. - D:\workspaces\exercise\cg_checkout_system\target>java -jar cg_checkout_system-0.0.1-SNAPSHOT-jar-with-dependencies.jar apple,orange,apple offers.conf
