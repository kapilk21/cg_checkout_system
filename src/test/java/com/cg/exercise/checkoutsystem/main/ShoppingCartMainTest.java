package com.cg.exercise.checkoutsystem.main;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class ShoppingCartMainTest {

	private ShoppingCartMain shoppingCartMain;

	@Before
	public void setUp() throws Exception {
		shoppingCartMain = new ShoppingCartMain();
	}
	
	@Test
	public void shouldValidateItemsWhenInTheCorrectFormat1(){
		//given
		String items = "Apple,Apple,Orange,Apple";
		//when
		boolean valid = ShoppingCartMain.isValid(items);
		//then
		assertTrue(valid);
	}
	
	@Test
	public void shouldValidateItemsWhenInTheCorrectFormat2(){
		//given
		String items = "Apple1,Apple2,Orange, Apple_2";
		//when
		boolean valid = ShoppingCartMain.isValid(items);
		//then
		assertTrue(valid);
	}
	
	@Test
	public void shouldValidateItemsWhenInTheCorrectFormat3(){
		//given
		String items = "Apple , Apple , Orange , Apple";
		//when
		boolean valid = ShoppingCartMain.isValid(items);
		//then
		assertTrue(valid);
	}
	
	@Test
	public void shouldFailValidationOfItemsWhenInvalidFormat1(){
		//given
		String items = "Apple,Apple,Orange,,Apple ";
		//when
		boolean valid = ShoppingCartMain.isValid(items);
		//then
		assertFalse(valid);
	}
	
	@Test
	public void shouldFailValidationOfItemsWhenInvalidFormat2(){
		//given
		String items = "Apple,Apple, , Orange";
		//when
		boolean valid = ShoppingCartMain.isValid(items);
		//then
		assertFalse(valid);
	}
	
	@Test
	public void shouldFailValidationOfItemsWhenBlank(){
		//given
		String items = " ";
		//when
		boolean valid = ShoppingCartMain.isValid(items);
		//then
		assertFalse(valid);
	}
	
}
