package com.cg.exercise.checkoutsystem.service;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cg.exercise.checkoutsystem.model.Item;
import com.cg.exercise.checkoutsystem.model.Offer;
import com.cg.exercise.checkoutsystem.model.OffersCache;
import com.cg.exercise.checkoutsystem.model.ShoppingCart;

@RunWith(MockitoJUnitRunner.class)
public class CheckOutServiceTest {

	@Mock
	private ShoppingCart shoppingCart;
	
	@Mock
	private OffersCache offers;
	
	@Mock
	private Offer offer;
	
	@InjectMocks
	private CheckOutService service=CheckOutService.INSTANCE;
	
	
	@Test
	public void shouldCheckOut() {
		// given
		List<Item> itemList= Arrays.asList(Item.APPLE, Item.ORANGE);
		when(shoppingCart.getItems()).thenReturn(itemList);
		when(offers.getOffer(Item.APPLE.name())).thenReturn(offer);
		when(offers.getOffer(Item.ORANGE.name())).thenReturn(offer);
		when(offer.apply(anyDouble(),anyInt())).thenReturn(0.60).thenReturn(0.25);
		when(shoppingCart.getTotal()).thenReturn(0.85);
		
		// when
		CheckOutService.INSTANCE.checkOut(shoppingCart);
		
		// then
		verify(offers,times(2)).getOffer(any(String.class));
		verify(shoppingCart).getItems();
		verify(shoppingCart).setTotal(0.60);
		verify(shoppingCart).setTotal(0.25);
	}
	
	@Test
	public void shouldCheckOutAndApplyAppleOffer() {
		// given
		List<Item> itemList= Arrays.asList(Item.APPLE, Item.APPLE, Item.ORANGE, Item.APPLE);
		when(shoppingCart.getItems()).thenReturn(itemList);
		when(offers.getOffer(Item.APPLE.name())).thenReturn(offer);
		when(offers.getOffer(Item.ORANGE.name())).thenReturn(offer);
		when(offer.apply(anyDouble(),anyInt())).thenReturn(1.20).thenReturn(0.25);
		when(shoppingCart.getTotal()).thenReturn(1.45);
		// when
		service.checkOut(shoppingCart);
		// then
		verify(offers,times(2)).getOffer(any(String.class));
		verify(shoppingCart).getItems();
		verify(shoppingCart).setTotal(1.20);
		verify(shoppingCart).setTotal(0.25);
	}
	
	@Test
	public void shouldCheckOutAndApplyOrangeOffer() {
		// given
		List<Item> itemList= Arrays.asList(Item.APPLE, Item.ORANGE, Item.ORANGE, Item.ORANGE);
		when(shoppingCart.getItems()).thenReturn(itemList);
		when(offers.getOffer(Item.APPLE.name())).thenReturn(offer);
		when(offers.getOffer(Item.ORANGE.name())).thenReturn(offer);
		when(offer.apply(anyDouble(),anyInt())).thenReturn(0.60).thenReturn(0.50);
		when(shoppingCart.getTotal()).thenReturn(1.10);
		// when
		service.checkOut(shoppingCart);
		// then
		verify(offers,times(2)).getOffer(any(String.class));
		verify(shoppingCart).getItems();
		verify(shoppingCart).setTotal(0.60);
		verify(shoppingCart).setTotal(0.50);
	}
	
}
