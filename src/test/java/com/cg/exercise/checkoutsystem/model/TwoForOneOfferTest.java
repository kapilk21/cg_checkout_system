package com.cg.exercise.checkoutsystem.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TwoForOneOfferTest {

	TwoForOneOffer twoForOneOffer;

	@Before
	public void setUp() throws Exception {
		twoForOneOffer = new TwoForOneOffer();
	}

	@Test
	public void shouldApplyOfferForItemCountInMultplesOfTwo() throws Exception {
		// given
		// when
		double actual = twoForOneOffer.apply(Item.APPLE.getValue(), 4);
		// then
		assertEquals(1.20, actual,0);
	}
	
	@Test
	public void shouldApplyOfferForItemCountNotInMultplesOfTwo() throws Exception {
		// given
		// when
		double actual = twoForOneOffer.apply(Item.APPLE.getValue(), 7);
		// then
		assertEquals(2.4, actual,0);
	}

}
