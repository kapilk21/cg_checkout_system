package com.cg.exercise.checkoutsystem.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThreeForThePriceOfTwoOfferTest {

	ThreeForThePriceOfTwoOffer threeForThePriceOfTwoOffer;   
	@Before
	public void setUp() throws Exception {
		threeForThePriceOfTwoOffer = new ThreeForThePriceOfTwoOffer();
	}

	@Test
	public void ShouldApplyOfferForMultipleOfThree() throws Exception {
		//given
		//when
		double actual = threeForThePriceOfTwoOffer.apply(Item.ORANGE.getValue(),6);
		//then
		assertEquals(1.0, actual,0);
	}
	
	@Test
	public void ShouldApplyOfferForNonMultiplesOfThree() throws Exception {
		//given
		//when
		double actual = threeForThePriceOfTwoOffer.apply(Item.ORANGE.getValue(),4);
		//then
		assertEquals(0.75, actual,0);
	}

}
