package com.cg.exercise.checkoutsystem.model;

import java.io.BufferedReader;
import java.io.FileReader;

import org.junit.Before; 
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(OffersCache.class)
public class OffersCacheTest {

	private OffersCache offersCache;
	
	@Before
	public void setUp() throws Exception {
		offersCache = new OffersCache();
	}
	
	@Test
	public void shouldLoadOffersFromFile() throws Exception{

		//given
		FileReader fr = mock(FileReader.class);
		BufferedReader br = mock(BufferedReader.class);
		ThreeForThePriceOfTwoOffer threeForThePriceOfTwoOffer = mock(ThreeForThePriceOfTwoOffer.class);
		TwoForOneOffer twoForOneOffer = mock(TwoForOneOffer.class);
		
		whenNew(FileReader.class).withAnyArguments().thenReturn(fr);
		whenNew(BufferedReader.class).withAnyArguments().thenReturn(br);
		whenNew(ThreeForThePriceOfTwoOffer.class).withNoArguments().thenReturn(threeForThePriceOfTwoOffer);
		whenNew(TwoForOneOffer.class).withNoArguments().thenReturn(twoForOneOffer);
		when(br.readLine()).thenReturn("APPLE=BUY_1_GET_1_FREE","ORANGE=3_FOR_THE_PRICE_OF_2").thenReturn(null);
		//when
		OffersCache.loadOffersCache("offers_conf_file");
		//then
		verifyNew(ThreeForThePriceOfTwoOffer.class).withNoArguments();
		verifyNew(TwoForOneOffer.class).withNoArguments();
		Offer actualAppleOffer = offersCache.getOffer("APPLE");
		Offer actualOrangeOffer = offersCache.getOffer("ORANGE");
		assertEquals(twoForOneOffer,actualAppleOffer);
		assertEquals(threeForThePriceOfTwoOffer, actualOrangeOffer);
	}

}
