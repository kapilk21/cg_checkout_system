package com.cg.exercise.checkoutsystem.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ShoppingCartTest {
	
	ShoppingCart shoppingCart;
	
	@Before
	public void setup(){
		shoppingCart = new ShoppingCart();
	}
	
	@Test
	public void shouldAddItemToCart() throws Exception {
	
		//given
		//when
		shoppingCart.add(Item.APPLE);
		shoppingCart.add(Item.ORANGE);
		//then
		assertEquals(2, shoppingCart.getItems().size());
	}
	
	@Test
	public void shouldAddItemListToCart() throws Exception {

		//given
		List<Item> itemList= new ArrayList<>( Arrays.asList(Item.APPLE, Item.APPLE, Item.ORANGE, Item.APPLE));
		//when
		shoppingCart.add(itemList);
		//then
		assertEquals(4, shoppingCart.getItems().size());
	}
	
	@Test
	public void shouldRemoveItemFromCart(){
	}
	

}
