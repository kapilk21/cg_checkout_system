package com.cg.exercise.checkoutsystem.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.cg.exercise.checkoutsystem.model.Item;
import com.cg.exercise.checkoutsystem.model.ShoppingCart;
import com.cg.exercise.checkoutsystem.service.ICheckOutService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ShoppingCartController.class)
public class ShoppingCartControllerTest {

	private static final String SCANNED_ITEMS = "Apple, Apple, Orange, Apple";
	
	@Mock
	private ShoppingCart shoppingCart;
	@Mock
	private ICheckOutService checkOutService;

	@InjectMocks
	private ShoppingCartController controller;
	
	@Test
	public void shouldAddItemsToShoppingCart(){
		//given
		when(shoppingCart.size()).thenReturn(4);
		//when
		controller.addItemsToShoppingCart(SCANNED_ITEMS,shoppingCart);
		//then
		verify(shoppingCart,times(4)).add(any(Item.class));
	}
	
	@Test
	public void shouldSkipUnknownItems(){
		//given
		String SCANNED_ITEMS = "Banana, Apple, Orange, Apple";
		when(shoppingCart.size()).thenReturn(3);
		//when
		controller.addItemsToShoppingCart(SCANNED_ITEMS, shoppingCart);
		//then
		verify(shoppingCart,times(3)).add(any(Item.class));
	}
	
	@Test
	public void shouldProcessItemsAndCheckOut() throws Exception{
		//given
		PowerMockito.whenNew(ShoppingCart.class).withNoArguments().thenReturn(shoppingCart);
		when(shoppingCart.size()).thenReturn(4);
		//when
		controller.process(SCANNED_ITEMS,null);
		//then
		PowerMockito.verifyNew(ShoppingCart.class).withNoArguments();
		verify(checkOutService).checkOut(shoppingCart);
	}

}
