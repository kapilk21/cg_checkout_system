package com.cg.exercise.checkoutsystem.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cg.exercise.checkoutsystem.model.Item;
import com.cg.exercise.checkoutsystem.model.OffersCache;
import com.cg.exercise.checkoutsystem.model.ShoppingCart;
import com.cg.exercise.checkoutsystem.service.ICheckOutService;

@Component
public class ShoppingCartController {

	private static final Logger logger = LoggerFactory.getLogger(ShoppingCartController.class);

	@Autowired
	ICheckOutService checkOutService;

	void addItemsToShoppingCart(String items, ShoppingCart shoppingCart) {

		logger.info("Adding follwing items to shopping cart {}", items);

		List<String> itemList = Arrays.asList(items.split("\\s*,\\s*"));

		for (String item : itemList) {

			logger.debug("Adding {}", item);
			if (StringUtils.isNotBlank(item)) {
				try {
					shoppingCart.add(Item.valueOf(item.trim().toUpperCase()));
					logger.debug("Added {}", item);
				} catch (IllegalArgumentException iae) {
					logger.info("Unknown Item {}... skipping this item", item);
					continue;
				}
			}
		}

		logger.info("Added {} items to shopping cart", shoppingCart.size());
	}

	public void process(String items, String offersFile) {
		logger.info("Processing items -> Adding to cart -> checkout");
		
		if(StringUtils.isNotBlank(offersFile)){
			logger.info("Using offers config file :: {}",offersFile);
			OffersCache.loadOffersCache(offersFile);
		}else{
			logger.info("Offers config file Not specified:: {}");
		}
		
		ShoppingCart shoppingCart = new ShoppingCart();
		addItemsToShoppingCart(items, shoppingCart);
		checkOutService.checkOut(shoppingCart);
		logger.info("Processed {} items", items);
	}

}
