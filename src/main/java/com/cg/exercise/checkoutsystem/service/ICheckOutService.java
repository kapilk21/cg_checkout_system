package com.cg.exercise.checkoutsystem.service;

import com.cg.exercise.checkoutsystem.model.ShoppingCart;

public interface ICheckOutService {

	void checkOut(ShoppingCart shoppingCart);
}
