package com.cg.exercise.checkoutsystem.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cg.exercise.checkoutsystem.model.Item;
import com.cg.exercise.checkoutsystem.model.Offer;
import com.cg.exercise.checkoutsystem.model.OffersCache;
import com.cg.exercise.checkoutsystem.model.ShoppingCart;

@Component
public enum CheckOutService implements ICheckOutService {

	INSTANCE;

	private static final Logger logger = LoggerFactory.getLogger(CheckOutService.class);

	@Autowired
	private OffersCache offers;

	public void checkOut(ShoppingCart shoppingCart) {

		List<Item> items = shoppingCart.getItems();
		logger.info("Checking out {} items", items.size());

		Map<String, List<Item>> collect = items.stream().collect(Collectors.groupingBy(Item::name));

		// check offers for item
		for (Entry<String, List<Item>> itemEntry : collect.entrySet()) {
			String itemName = itemEntry.getKey();
			Offer offer = offers.getOffer(itemName);
			
			if(offer != null){
				logger.info("offer {} found for item {}",offer.getClass().getName(),itemName);
				List<Item> itemList = itemEntry.getValue();
				double total= offer.apply(itemList.get(0).getValue(),itemList.size());
				shoppingCart.setTotal(total);
			}else{
				logger.info("No offers found for item {} at this time",itemName);
				double total = 0;
				for (Item item : itemEntry.getValue()) {
					total+=item.getValue();
				}
				logger.info("total for item {} is {}",itemName,total);
				shoppingCart.setTotal(total);
			}
		}
		logger.info("Shopping Cart total {}", shoppingCart.getTotal());
	}
}
