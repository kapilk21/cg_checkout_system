package com.cg.exercise.checkoutsystem.model;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

	private List<Item> items= new ArrayList<>();
	private double total;
	
	public void add(Item item){
		items.add(item);
	}
	
	public void add(List<Item> items){
		this.items.addAll(items);
	}
	
	public List<Item> getItems(){
		return items;
	}

	public void setTotal(double total) {
		this.total+=total;
	}

	public double getTotal() {
		return total;
	}

	public int size() {
		return items.size();
	}
	
}
