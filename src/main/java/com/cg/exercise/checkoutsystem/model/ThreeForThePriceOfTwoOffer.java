package com.cg.exercise.checkoutsystem.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreeForThePriceOfTwoOffer implements Offer {

	private static final Logger logger = LoggerFactory.getLogger(ThreeForThePriceOfTwoOffer.class);

	@Override
	public double apply(double itemValue, int numberOfItems) {

		double total = 0;
		logger.info("Original Size:: {}", numberOfItems);

		int count = numberOfItems%3 + numberOfItems/3*2;
		total = itemValue* count;
		
		logger.info("total:: {}", total);
		return total;
	}

}
