package com.cg.exercise.checkoutsystem.model;

public enum Item {

	APPLE(0.60),
	ORANGE(0.25);
	
	double price;
	
	private Item(double price){
		this.price = price;
	}
	
	public double getValue(){
		return price;
	}
}
