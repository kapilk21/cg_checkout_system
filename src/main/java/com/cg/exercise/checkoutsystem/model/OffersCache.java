package com.cg.exercise.checkoutsystem.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OffersCache {

	private static final Logger logger = LoggerFactory.getLogger(OffersCache.class);
	private static final String BUY_ONE_GET_ONE_FREE = "BUY_1_GET_1_FREE";
	private static final String THREE_FOR_THE_PRICE_OF_TWO = "3_FOR_THE_PRICE_OF_2";
	private static Map<String, Offer> offersMap = new HashMap<>();

	public static void loadOffersCache(String offersFile){
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(offersFile));
		} catch (FileNotFoundException fnfe) {
			logger.error("File Not Found {}", offersFile);
			return;
		}
		String sCurrentLine;
		try {
			while ((sCurrentLine = br.readLine()) != null) {
				String[] offerLine = sCurrentLine.split("=");
				String offerName = offerLine[1];
				logger.info("Offer name {}",offerName);
				if (THREE_FOR_THE_PRICE_OF_TWO.equalsIgnoreCase(offerName)) {
					offersMap.put(offerLine[0], new ThreeForThePriceOfTwoOffer());
				} else if (BUY_ONE_GET_ONE_FREE.equalsIgnoreCase(offerName)) {
					offersMap.put(offerLine[0], new TwoForOneOffer());
				}
			}
		} catch (IOException ioe) {
			logger.error(ioe.getMessage());
		}
	}

	public Offer getOffer(String item) {
		return offersMap.get(item);
	}

}
