package com.cg.exercise.checkoutsystem.model;

public interface Offer {

	double apply(double itemValue, int numberOfItems);
}
