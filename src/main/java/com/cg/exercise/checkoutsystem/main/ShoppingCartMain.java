package com.cg.exercise.checkoutsystem.main;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import com.cg.exercise.checkoutsystem.controller.ShoppingCartController;

public class ShoppingCartMain {

	
	private static final Logger logger =  LoggerFactory.getLogger(ShoppingCartMain.class);
	private static String offersFile;
	
	public static void main(String[] args) {
		
		int argsCount = args.length;
		
		if(argsCount <=0){
			logger.info("Nothing to checkout");
			return;
		}
		
		String items = StringUtils.trim(args[0]);

		if(argsCount==2){
			offersFile = StringUtils.trim(args[1]);
		}
		logger.info("Validating items {}", items);
		
		if(isValid(items)){
			processItems(items,offersFile);
		}else{
			logger.info("Exiting...Invalid Input received");
		}
	}

	private static void processItems(String items, String offersFile) {
		
		logger.info("Intializing ShoppingCartConfig context");
		ApplicationContext context = new AnnotationConfigApplicationContext(ShoppingCartConfig.class);
		ShoppingCartController controller = context.getBean(ShoppingCartController.class);
		controller.process(items,offersFile);
		((AbstractApplicationContext)context).registerShutdownHook();
	}

	static boolean isValid(String items) {
		
		if(StringUtils.isNotBlank(items)){
			logger.info("Received items {}",items);
			if(items.matches("\\w+(\\s*,\\s*\\w+)*")){
				logger.info("Valid Input received");
				return true;
			}else{
				logger.info("Incorrect format");
				return false;
			}
		}else{
			logger.info("Received blank items");
			return false;
		}
	}

}
