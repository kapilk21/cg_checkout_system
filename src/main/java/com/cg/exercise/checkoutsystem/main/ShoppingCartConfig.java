package com.cg.exercise.checkoutsystem.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cg.exercise.checkoutsystem.controller.ShoppingCartController;
import com.cg.exercise.checkoutsystem.model.OffersCache;
import com.cg.exercise.checkoutsystem.service.CheckOutService;

@Configuration
public class ShoppingCartConfig {

	@Bean(name="shoppingCartController")
	public ShoppingCartController shoppingCartController(){
		return new ShoppingCartController();
	}
	
	@Bean(name="checkOutService")
	public CheckOutService checkOutService(){
		return CheckOutService.INSTANCE;
	}
	
	@Bean(name="offers")
	public OffersCache offers(){
		return new OffersCache();
	}
	
}
